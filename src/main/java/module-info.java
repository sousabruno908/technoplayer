module technoplayer {
    requires javafx.controls;
    requires javafx.fxml;
    
    opens br.ufc.tecprog.technoplayer to javafx.fxml;
    exports br.ufc.tecprog.technoplayer;
}
