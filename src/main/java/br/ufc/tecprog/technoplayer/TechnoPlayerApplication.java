package br.ufc.tecprog.technoplayer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TechnoPlayerApplication extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new StackPane());
        stage.setTitle("TechnoPlayer");
        stage.setWidth(800);
        stage.setHeight(480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(TechnoPlayerApplication.class, args);
    }

}
